#!/usr/bin/make -f
# -*- makefile -*-

## Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

export PYBUILD_NAME=lmfit

export PYBUILD_BEFORE_TEST=cp -r NIST_STRD examples {build_dir}
export PYBUILD_AFTER_TEST=rm -fr {build_dir}/NIST_STRD {build_dir}/examples; find {build_dir} -name "*.sav" -delete

export MPLCONFIGDIR=/var/tmp

%:
	dh $@ --buildsystem=pybuild

override_dh_clean:
	find . -name "*.pickle" -delete
	rm -rf doc/examples
	if [ -f lmfit/version.py.orig ] ; then mv lmfit/version.py.orig lmfit/version.py ; fi
	dh_clean

override_dh_auto_configure:
	cp -a lmfit/version.py lmfit/version.py.orig
	dh_auto_configure

override_dh_installdocs-indep:
	mv examples/example_emcee_Model_interface.py examples/nodoc_example_emcee_Model_interface.py
	cp doc/sphinx/ext_mathjax.py doc/extensions.py
	PYTHONPATH=. http_proxy='localhost' python3 -m sphinx -N -bhtml -D mathjax_path="/usr/share/javascript/mathjax/MathJax.js" doc/ .pybuild/html  # HTML generator
	rm doc/extensions.py
	mv examples/nodoc_example_emcee_Model_interface.py examples/example_emcee_Model_interface.py
	dh_installdocs -i
	dh_installdocs .pybuild/html -p python-lmfit-doc --doc-main-package=python3-lmfit

execute_before_dh_python3:
	rm -rf debian/python3-lmfit/usr/lib/python3*/dist-packages/htmlcov debian/python3-lmfit/usr/lib/python3*/dist-packages/.coverage
