Source: lmfit-py
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Picca Frédéric-Emmanuel <picca@debian.org>,
 Michael Hudson-Doyle <mwhudson@debian.org>,
Section: science
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-numpy3,
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 pybuild-plugin-pyproject,
 python3-all,
 python3-asteval (>= 1.0~),
 python3-dill <!nocheck>,
 python3-ipython <!nodoc>,
 python3-nbsphinx <!nodoc>,
 python3-numpy (>= 1.19~),
 python3-pandas,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-scipy (>= 1.6~),
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx <!nodoc>,
 python3-sphinx-gallery <!nodoc>,
 python3-sympy <!nodoc>,
 python3-uncertainties (>= 3.2.2~),
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/lmfit-py
Vcs-Git: https://salsa.debian.org/science-team/lmfit-py.git
Homepage: https://lmfit.github.io/lmfit-py/
Rules-Requires-Root: no

Package: python3-lmfit
Architecture: all
Section: python
Depends:
 python3-asteval (>= 1.0~),
 python3-uncertainties (>= 3.2.2~),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-lmfit-doc,
Description: Least-Squares Minimization with Constraints (Python 3)
 The lmfit Python package provides a simple, flexible interface to
 non-linear optimization or curve fitting problems. The package
 extends the optimization capabilities of scipy.optimize by replacing
 floating pointing values for the variables to be optimized with
 Parameter objects. These Parameters can be fixed or varied, have
 upper and/or lower bounds placed on its value, or written as an
 algebraic expression of other Parameters.
 .
 The principal advantage of using Parameters instead of simple
 variables is that the objective function does not have to be
 rewritten to reflect every change of what is varied in the fit, or
 what relationships or constraints are placed on the Parameters. This
 means a scientific programmer can write a general model that
 encapsulates the phenomenon to be optimized, and then allow user of
 that model to change what is varied and fixed, what range of values
 is acceptable for Parameters, and what constraints are placed on the
 model. The ease with which the model can be changed also allows one
 to easily test the significance of certain Parameters in a fitting
 model.
 .
 The lmfit package allows a choice of several optimization methods
 available from scipy.optimize. The default, and by far best tested
 optimization method used is the Levenberg-Marquardt algorithm from
 MINPACK-1 as implemented in scipy.optimize.leastsq. This method
 is by far the most tested and best support method in lmfit, and much
 of this document assumes this algorithm is used unless explicitly
 stated. An important point for many scientific analysis is that this
 is only method that automatically estimates uncertainties and
 correlations between fitted variables from the covariance matrix
 calculated during the fit.
 .
 A few other optimization routines are also supported, including
 Nelder-Mead simplex downhill, Powell's method, COBYLA, Sequential
 Least Squares methods as implemented in scipy.optimize.fmin, and
 several others from scipy.optimize. In their native form, some of
 these methods setting allow upper or lower bounds on parameter
 variables, or adding constraints on fitted variables. By using
 Parameter objects, lmfit allows bounds and constraints for all of
 these methods, and makes it easy to swap between methods without
 hanging the objective function or set of Parameters.
 .
 Finally, because the approach derived from MINPACK-1 usin the
 covariance matrix to determine uncertainties is sometimes questioned
 (and sometimes rightly so), lmfit supports methods to do a brute
 force search of the confidence intervals and correlations for sets of
 parameters.
 .
 This is the Python 3 version of the package.

Package: python-lmfit-doc
Architecture: all
Section: doc
Depends:
 libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Built-Using:
 ${sphinxdoc:Built-Using},
Multi-Arch: foreign
Description: Least-Squares Minimization with Constraints (Documentation)
 The lmfit Python package provides a simple, flexible interface to
 non-linear optimization or curve fitting problems. The package
 extends the optimization capabilities of scipy.optimize by replacing
 floating pointing values for the variables to be optimized with
 Parameter objects. These Parameters can be fixed or varied, have
 upper and/or lower bounds placed on its value, or written as an
 algebraic expression of other Parameters.
 .
 The principal advantage of using Parameters instead of simple
 variables is that the objective function does not have to be
 rewritten to reflect every change of what is varied in the fit, or
 what relationships or constraints are placed on the Parameters. This
 means a scientific programmer can write a general model that
 encapsulates the phenomenon to be optimized, and then allow user of
 that model to change what is varied and fixed, what range of values
 is acceptable for Parameters, and what constraints are placed on the
 model. The ease with which the model can be changed also allows one
 to easily test the significance of certain Parameters in a fitting
 model.
 .
 The lmfit package allows a choice of several optimization methods
 available from scipy.optimize. The default, and by far best tested
 optimization method used is the Levenberg-Marquardt algorithm from
 MINPACK-1 as implemented in scipy.optimize.leastsq. This method
 is by far the most tested and best support method in lmfit, and much
 of this document assumes this algorithm is used unless explicitly
 stated. An important point for many scientific analysis is that this
 is only method that automatically estimates uncertainties and
 correlations between fitted variables from the covariance matrix
 calculated during the fit.
 .
 A few other optimization routines are also supported, including
 Nelder-Mead simplex downhill, Powell's method, COBYLA, Sequential
 Least Squares methods as implemented in scipy.optimize.fmin, and
 several others from scipy.optimize. In their native form, some of
 these methods setting allow upper or lower bounds on parameter
 variables, or adding constraints on fitted variables. By using
 Parameter objects, lmfit allows bounds and constraints for all of
 these methods, and makes it easy to swap between methods without
 hanging the objective function or set of Parameters.
 .
 Finally, because the approach derived from MINPACK-1 usin the
 covariance matrix to determine uncertainties is sometimes questioned
 (and sometimes rightly so), lmfit supports methods to do a brute
 force search of the confidence intervals and correlations for sets of
 parameters.
 .
 This is the common documentation package.
